/* eslint-disable import/prefer-default-export */


export default {
  first_name: { required: true, label: 'Имя', suggestions: { type: "NAME", part: "NAME"} },
  last_name: { required: true, label: 'Фамилия', suggestions: { type: "NAME", part: "SURNAME"} },
  patronymic: { label: 'Отчество', suggestions: { type: "NAME", part: "PATRONYMIC"} },
  sex: {
    label: 'Пол',
    type: 'select',
    options: [
      { value: 1, label: 'Мужской' },
      { value: 2, label: 'Женский' }
    ]
  },
  birth_date: { required: true, label: 'Дата рождения', type: 'date' },
  phone: { required: true, label: 'Мобильный телефон', type: 'phone' },
  email: {
    label: 'Email',
    mask: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i,
    maskError: 'Введен некорректный адрес почты',
  },
  address: { label: 'Адрес постоянной регистрации', suggestions: { type: "ADDRESS" } },
  employer: { label: 'Название работодателя', suggestions: { type: "PARTY" } },
};
