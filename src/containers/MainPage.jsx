import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from '../actions';
import Layout from '../components/Layout';

class MainPage extends Component {
  render() {
    return (
      <Layout {...this.props} />
    );
  }
}


const mapStateToProps = state => ({
  ...state,
});

const mapActions = {
  changeUserData: actions.changeUserData,
};

const MainPageHOC = connect(mapStateToProps, mapActions)(MainPage);

export default MainPageHOC;
