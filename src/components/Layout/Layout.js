import React, { Component } from 'react';
import PropTypes from 'prop-types';
import formFields from '../../constants/formFields';
import isEmptyObject from '../../utils/isEmptyObject';
import icons from './assets/svg-icons';
import FormInput from '../FormInput';
import './Layout.scss';


class Layout extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    changeUserData: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      user: props.user,
      errors: {},
    };
    this.collectData = this.collectData.bind(this);
    this.onSaveClickHandler = this.onSaveClickHandler.bind(this);
  }

  collectData(field, value) {
    const user = { ...this.state.user };
    user[field] = value;
    this.setState({
      user,
    });
  }

  validate() {
    const user = { ...this.state.user };
    const errors = {};
    Object.keys(formFields).forEach((field) => {
      if (!user[field]) {
        if (formFields[field].required) {
          errors[field] = 'Поле является обязательным';
        }
      } else if (formFields[field].mask && !formFields[field].mask.test(user[field])) {
        errors[field] = formFields[field].maskError || 'Введено некорректное значение';
      }
    })
    this.setState({
      errors,
    });
    return isEmptyObject(errors);
  }

  onSaveClickHandler() {
    const { changeUserData } = this.props;
    const { user } = this.state;
    const isValidForm = this.validate();
    if (isValidForm && changeUserData) {
      alert('Форма валидна, отправляется запрос');
      changeUserData(user);
    }
  }

  render() {
    const { user, errors } = this.state;
    const inputs = {};
    Object.keys(formFields).forEach((field) => {
      const componentProps = {
        field,
        value: user[field],
        error: errors[field],
        formFieldProperties: formFields[field],
        sendData: this.collectData,
      }
      inputs[field] = (
        <FormInput {...componentProps} />
      );
    });
    return (
      <div className="pageContent">
        <div style={{ display: 'none' }} dangerouslySetInnerHTML={{ __html: icons }} />
        <h1>Информация о сотруднике</h1>
        <div className="form">
          <div className="row">
            {inputs.first_name}
          </div>
          <div className="row">
            {inputs.last_name}
            {inputs.patronymic}
          </div>
          <div className="row">
            {inputs.sex}
            {inputs.birth_date}
          </div>
          <div className="row">
              {inputs.phone}
              {inputs.email}
          </div>
          <div className="row">
            {inputs.address}
          </div>
          <div className="row">
              {inputs.employer}
          </div>
          <div className="right-align">
            <button className="submit-btn" onClick={this.onSaveClickHandler}>Сохранить</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Layout;
