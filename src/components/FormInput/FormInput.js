import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import DatePicker from 'react-date-picker';
import Select from 'react-select';
import config from '../../config';
import isEmptyObject from '../../utils/isEmptyObject';
import './FormInput.scss';

class FormInput extends Component {
  static propTypes = {
    value: PropTypes.any,
    field: PropTypes.string,
    error: PropTypes.string,
    sendData: PropTypes.func,
    formFieldProperties: PropTypes.object,
  };

  static get defaultProps() {
    return {
      value: '',
      field: '',
      error: '',
      sendData: null,
      formFieldProperties: {},
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      currentValue: props.value,
    };
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.changeStateHandler = this.changeStateHandler.bind(this);
    this.onSelectSuggestion = this.onSelectSuggestion.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.renderDatePicker = this.renderDatePicker.bind(this);
    this.renderPhoneInput = this.renderPhoneInput.bind(this);
    this.renderSelect = this.renderSelect.bind(this);
  }

  componentDidMount() {
    const { formFieldProperties, field } = this.props;
    if (!formFieldProperties.suggestions || isEmptyObject(formFieldProperties.suggestions)) {
      return;
    }
    const $ = window.jQuery;
    const suggestionsProps = {
      token: config.token,
      type: formFieldProperties.suggestions.type,
      count: 5,
      onSelect: this.onSelectSuggestion,
      onSearchError: () => {
       $("#message").text("Подсказки не работают"); 
      }
    };
    if (formFieldProperties.suggestions.part) {
      suggestionsProps.params = { parts: [formFieldProperties.suggestions.part] };
    }
    $(`#${field}`).suggestions({...suggestionsProps});
  }

  onSelectSuggestion(suggestion) {
    this.changeStateHandler(suggestion.value);
  }

  onBlurHandler() {
    const { sendData, field } = this.props;
    if (sendData) {
      sendData(field, this.state.currentValue);
    }
  }

  onChangeInputHandler(event) {
    this.changeStateHandler(event.target.value);
  }

  changeStateHandler(value) {
    this.setState({
      currentValue: value,
    });
  }

  onChangeDate(value) {
    const { sendData, field } = this.props;
    this.changeStateHandler(value);
    if (sendData) {
      sendData(field, value);
    }
  }

  renderDatePicker() {
    const { formFieldProperties, field, error } = this.props;
    const { currentValue } = this.state;
    const today = new Date();
    const dateValue = currentValue ? currentValue : undefined;
    const errorBlock = (<span className="error-text">{error}</span>);
    const labelClass = currentValue ? 'active' : '';
    return (
      <div className="user-data-input date-input">
        <DatePicker
          id={field}
          value={dateValue}
          maxDate={today}
          onChange={this.onChangeDate}
          className={`calendar-input ${error ? 'invalid' : ''}`}
          calendarClassName="calendar-panel"
          placeholder={formFieldProperties.label}
          locale="ru-RU"
          calendarIcon={<svg><use xlinkHref="#icon-calendar" /></svg>}
          clearIcon={<svg><use xlinkHref="#icon-close" /></svg>}
          showLeadingZeros
        />
        <label htmlFor={field} className={labelClass}>{formFieldProperties.label}</label>
        {error && errorBlock}
      </div>
    );
  }

  renderPhoneInput() {
    const { formFieldProperties, field, error } = this.props;
    const { currentValue } = this.state;
    const labelClass = /\d+/.test(currentValue) ? 'active' : '';
    const errorBlock = (<span className="error-text">{error}</span>);
    return (
      <div className="user-data-input phone-input">
        <InputMask
          id={field}
          className={`text-input ${error ? 'invalid' : ''}`}
          maskChar="_"
          mask="(999)999-99-99"
          value={currentValue}
          onBlur={this.onBlurHandler}
          onChange={this.onChangeInputHandler}
        />
        <label htmlFor={field} className={labelClass}>{formFieldProperties.label}</label>
        {error && errorBlock}
      </div>
    );
  }

  renderSelect() {
    const { formFieldProperties, field, error } = this.props;
    const { currentValue } = this.state;
    const options = formFieldProperties.options.map(item => (
      { value: item.value, label: item.label }
    ));
    const labelClass = currentValue ? 'active' : '';
    const errorBlock = (<span className="error-text">{error}</span>);
    return (
      <div className="user-data-input">
        <Select
          id={field}
          value={currentValue}
          onBlur={this.onBlurHandler}
          onChange={this.changeStateHandler}
          options={options}
          placeholder=""
          className={error ? 'invalid' : ''}
        />
        <label htmlFor={field} className={labelClass}>{formFieldProperties.label}</label>
        {error && errorBlock}
      </div>
    );
  }

  render() {
    const { formFieldProperties, field, error } = this.props;
    const { currentValue } = this.state;
    switch (formFieldProperties.type) {
      case 'date':
        return this.renderDatePicker();
      case 'phone':
        return this.renderPhoneInput();
      case 'select':
        return this.renderSelect();
      default: {
        const containerAddClass = field === 'employer' ? "employer-input" : '';
        const errorBlock = (<span className="error-text">{error}</span>);
        const labelClass = currentValue ? 'active' : '';
        return (
          <div className={`user-data-input ${containerAddClass}`}>
            <input
              id={field} 
              type="text"
              className={`text-input ${error ? 'invalid' : ''}`}
              value={currentValue}
              onBlur={this.onBlurHandler}
              onChange={this.onChangeInputHandler}
            />
            <label htmlFor={field} className={labelClass}>{formFieldProperties.label}</label>
            {error && errorBlock}
          </div>
        );
      }
    }
  }
}

export default FormInput;
