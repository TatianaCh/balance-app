import * as ActionTypes from '../constants';
import formFields from '../constants/formFields';

const initialState = {};
Object.keys(formFields).forEach((field) => {
  initialState[field] = '';
});

export default function user(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.CHANGE_USER_DATA:
      {
        return { ...state, ...action.payload };
      }
    default:
      return state;
  }
};
