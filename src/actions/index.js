import * as ActionTypes from '../constants';

function changeUserData(data) {
  return {
      type: ActionTypes.CHANGE_USER_DATA,
      payload: data,
    };
};

export default {
  changeUserData,
};

